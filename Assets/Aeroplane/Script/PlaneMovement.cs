﻿using UnityEngine;
using System.Collections;

public class PlaneMovement : MonoBehaviour
{
    //public Transform refObj;
    //public Transform axisPoint;

    public float speed;
    public Vector3 moveDir;
    Rigidbody rbody;
	// Use this for initialization
	void Start () 
    {
       // refferenceAxis = refObj.transform
        rbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        moveDir = new Vector3(Input.GetAxisRaw("Horizontal"),0,Input.GetAxisRaw("Vertical")).normalized;
        //transform.RotateAround(refObj.position,axisPoint.position , 20 * Time.deltaTime);
   	}

    void FixedUpdate()
    {
        rbody.MovePosition(rbody.position + transform.TransformDirection(moveDir) * speed* Time.deltaTime);  
    }
}
