﻿#define LIGHTSPEED
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WPM
{
	public class Movingmap : MonoBehaviour 
	{

		WorldMapGlobe map;
		bool animatingField;
		float zoomLevel = 1.0f;

		void Awake ()
		{
			// Get a reference to the World Map API:
			map = WorldMapGlobe.instance;

#if LIGHTSPEED
	Camera.main.fieldOfView = 180;
	animatingField = true;
#endif

		}

		void Start () 
		{
			
		}

		// Update is called once per frame
		void Update () 
		{

			// Animates the camera field of view (just a cool effect at the begining)
			if (animatingField) {
				if (Camera.main.fieldOfView > 60) {
					Camera.main.fieldOfView -= (181.0f - Camera.main.fieldOfView) / (220.0f - Camera.main.fieldOfView); 
				} else {
					Camera.main.fieldOfView = 60;
					animatingField = false;
				}
			}

			GUIResizer.AutoResize ();

		}
	}
}

